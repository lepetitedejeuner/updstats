#!/usr/bin/env python3
try:
    from pdfminer.converter import TextConverter
    from pdfminer.layout import LAParams
    from pdfminer.pdfdocument import PDFDocument
    from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
    from pdfminer.pdfpage import PDFPage
    from pdfminer.pdfparser import PDFParser
except ImportError as e:
    print("{}.  Try: pip3 install -r requirements.txt".format(str(e)))
    from sys import exit
    exit(1)

from argparse import ArgumentParser
from datetime import datetime
from io import BytesIO, StringIO
from logging import debug, info, warning, error, critical
from re import match, split, sub

class Person():
    def __init__(self, age=None, sex=None, race=None, height=None, weight=None,
                 hair=None, eyes=None, location=None, name=None):
        self.age = int(age) if age else age
        self.sex = sex
        self.race = race
        self.height = height
        self.weight = weight
        self.hair = hair
        self.eyes = eyes
        self.location = location
        self.name = name

    def __str__(self):
        return ("Sex: {}, Age: {}, Race: {}, Height: {}, Weight: {}, Hair: {},"
                " Eyes: {}, Location: {}").format(self.sex, self.age,
               self.race, self.height, self.weight, self.hair, self.eyes,
               self.location)

    def __repr__(self):
        return self.__str__()

class Arrest():
    def __init__(self, arrestee=None, charge=None, at=None, officer=None):
        self.arrestee = arrestee  # Person object
        self.charge = charge
        self.at = at
        self.officer = officer  # Person object

    def __str__(self):
        return "Arrestee: {}, Charge: {}, At: {}, Officer: {}".format(
               str(self.arrestee), self.charge, self.at, str(self.officer))

    def __repr__(self):
        return self.__str__()

class Report():
    def __init__(self, case_number, charges, location, occurred, reported,
                 officer, summary, extras):
        self.case_number = case_number.strip()
        self.charges = []
        for line in [x.strip() for x in charges.strip().split("\n")]:
            self.charges.append(self.extract_charge(line))
        self.location = location.strip()
        self.occurred = datetime.strptime(occurred.strip(), "%m/%d/%Y %H:%M")
        self.reported = datetime.strptime(reported.strip(), "%m/%d/%Y %H:%M")
        self.officer = Person(name=officer.strip())
        self.summary = summary.strip()
        self.property = None
        self.offenders = []
        self.victims = []
        self.arrests = []
        self.extras = extras  # Extra sections that aren't being parsed yet
        self._extract_property()
        self._extract_people()
        self._extract_arrests()

    def _extract_property(self):
        """
        Extracts property information from self.extras
        """
        for extra in self.extras:
            parts = split(r"PROPERTY:", extra.strip(), 1)
            if len(parts) > 1:  # There is property information
                if parts[0] == "":
                    self.property = parts[1].strip()
                else:
                    warning("Property info found but not extracted. Case {}"
                            .format(self.case_number))

    def _extract_people(self):
        """
        Extracts people information from self.extras
        """
        for extra in self.extras:
            # Sometimes there's a PEOPLE: section, so first we deal with that
            people_section, extra = self._extract_regex(r"PEOPLE: (\d+)", extra)
            if people_section:  # There is people information
                self._extract_offenders_and_victims(people_section.strip())
            else: # There wasn't a PEOPLE section, but we check for offenders
                # and victims anyway, because sometimes the section header is
                # missing
                self._extract_offenders_and_victims(extra.strip())

    def _extract_offenders_and_victims(self, extra):
        """
        Extracts offender and victim information from the provided extra section

        :param extra: Section potentially containing offender & victim info
        :type extra: String
        :returns: None
        :rtype: None
        """
        offender_text = []
        victim_text = []
        for line in extra.split("\n"):
            if line.strip().startswith("OFFENDER"):
                offender_text.append(line.split("OFFENDER")[1].strip())
            if line.strip().startswith("VICTIM"):
                victim_text.append(line.split("VICTIM")[1].strip())
        for offender in offender_text:
            self.offenders.append(self._extract_person(offender))
        for victim in victim_text:
            self.victims.append(self._extract_person(victim))

    def _extract_person(self, info):
        """
        Extracts information about a person from the info provided

        :param info: Section containing info about a person (sex, age, etc.)
        :type info: String
        :returns: Object containing all the extractable info about this person
        :rtype: `py:Person` object
        """
        age, info = self._extract_regex(r".*AGE: (\d+)", info)
        sex, info = self._extract_regex(r".*SEX: (.)", info)
        race, info = self._extract_regex(r".*RACE: (\S*)", info)
        height, info = self._extract_regex(r".*HEIGHT: (\S*)", info)
        weight, info = self._extract_regex(r".*WEIGHT: (\S*)", info)
        hair, info = self._extract_regex(r".*HAIR: (\S*)", info)
        eyes, location = self._extract_regex(r".*EYES: ?(\S*)", info)
        return Person(age, sex, race, height, weight, hair, eyes, location)

    def _extract_regex(self, regex, haystack):
        """
        Extracts the first regex match from the haystack and removes the
        matched text from the haystack.

        :param regex: Regex to match, must contain at least 1 group
                      For example: r".*SEX: *(.)"
        :type regex: regex String
        :param haystack: the text to search
        :type haystack: String
        :returns: Tuple(value matching the group, haystack with text removed)
        :rtype: Tuple(String, String)
        """
        value = None
        m = match(regex, haystack)
        if m:
            value = m.groups()[0].strip()
            haystack = m.string.replace(m.group(), "")
        return value, haystack

    def extract_charge(self, line):
        """
        Extracts the code from the line that has both the charge and the
        description of the charge (or possibly just text and no code at
        all, as is the case with lost property).
        """
        m = match(r"(.*?)(\d+[\-/\.\d]+)(.*)", line)
        if m == None:  # No code found
            return (line, None)
        parts = m.groups()
        return (parts[0] + " " + parts[2], parts[1])

    def _extract_arrest(self, arrest_info):
        """
        Extracts information about a single arrest from the provided info.  If
        someone is arrested for multiple charges, this function will be called
        multiple times.

        :param arrest_info: The three lines of arrest info
        :type arrest_info: List of Strings
        :returns: Parsed arrest information
        :rtype: `py:Arrest` object
        """
        # The first line should have <name> AGE... (other person properties)
        name = arrest_info[0].split("AGE: ")[0].strip()
        arrestee = self._extract_person(arrest_info[0].replace(name, ""))
        arrestee.name = name
        # The second line should have CHARGE: <charge info>
        charge, _ = self._extract_regex(r".*CHARGE: (.*)", arrest_info[1].strip())
        charge_parts = self.extract_charge(charge)
        # The third line should have AT: <location> BY: <officer>
        at, officer = self._extract_regex(r".*AT: (.*)BY: ", arrest_info[2].strip())
        return Arrest(arrestee, charge_parts, at, Person(name=officer))

    def _extract_arrests(self):
        """
        Extracts all the arrests from the extras section which has arrest info,
        if such a section exists for this report.
        """
        for extra in self.extras:
            parts = split(r"ARRESTS: ", extra.strip())
            if len(parts) > 1:
                lines = parts[1].split("\n")
                # Each arrest record is three lines of text
                for i in range(0, len(lines), 3):
                    self.arrests.append(self._extract_arrest(lines[i:i+3]))

class ReportParser():
    def __init__(self, input_data):
        """
        Constructor takes an entire PDF's worth of data

        :param pdf_text_clean: The report text with page headers stripped
        :type pdf_text_clean: String
        :returns: Unparsed data
        :rtype: String
        """
        self.input_data = input_data
        self.reports = []
        self._split_reports()
        debug("Ingested {} reports".format(len(self.reports)))

    def _split_reports(self):
        """
        Splits the entire PDF into individual reports
        """
        #debug("Full PDF = %s" % self.input_data)
        parts = split(r"(\n\n U\d\d-\d+\w+)", "\n\n "+self.input_data)[1:]
        for i in range(1, len(parts), 2):
            self.reports.append(self._parse_report(parts[i-1], parts[i]))

    def _parse_report(self, case_number, report_text):
        """
        Parses one report into a Report object

        :param case_number: The case number
        :type case_number: String
        :param report_text: The text of the report which should be parsed
        :type report_text: String
        :returns: Parsed report
        :rtype: `py:Report` object
        """
        debug("report_text = %s" % report_text)
        # There are a number of fields which are always present and always
        # occur in a specific order, which makes parsing much easier
        charges, remainder = split(r"LOCATION:", report_text, 1)
        #debug("charges = %s" % charges)
        location, remainder = split(r"OCCURRED:", remainder, 1)
        #debug("location = %s" % location)
        occurred, remainder = split(r"REPORTED:", remainder, 1)
        reported, remainder = split(r"OFFICER:", remainder, 1)

        # Sections are separated with a double newline, fields after the
        # officer are not consistent, but we attempt to parse the summary since
        # that is usually present, and will worry about parsing the rest later
        parts = split(r"SUMMARY:", remainder, 1)
        if len(parts) > 1:  # If we have a summary
            officer, remainder = split(r"SUMMARY:", remainder, 1)
            remainder = split(r"\n\n", remainder)
            summary = remainder[0].strip()
        else:  # There's no summary, just fill in the officer
            remainder = split(r"\n\n", remainder)
            officer = remainder[0]
            summary = ""
        # The first remainder is either the summary or the officer, which
        # we have already captured, so we strip it from the list of remainders
        remainder = remainder[1:]

        return Report(case_number, charges, location, occurred, reported,
                      officer, summary, remainder)

def get_text_from_pdf(pdf_content):
    """
    Extract the text content from the PDF

    :param pdf_content: The PDF bytes
    :type pdf_content: bytes
    :returns: The text from the PDF
    :rtype: String
    """
    output_string = StringIO()
    parser = PDFParser(BytesIO(pdf_content))
    doc = PDFDocument(parser)
    rsrcmgr = PDFResourceManager()
    device = TextConverter(rsrcmgr, output_string, laparams=LAParams())
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    for page in PDFPage.create_pages(doc):
        interpreter.process_page(page)
    return output_string.getvalue()

def strip_headers(pdf_text):
    """
    Strips out the page headers, so we only see report data.

    :param pdf_text: The text extracted from the PDF
    :type pdf_text: String
    :returns: The text from the PDF, sans page headers
    :rtype: String
    """
    return sub("PRESS RELEASE.*", "", sub('POL.*', "", pdf_text)).strip()


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('file', nargs="+")
    args = parser.parse_args()

    for f in args.file:
        with open(f, "rb") as f:
            pdf_data = f.read()
        pdf_text = get_text_from_pdf(pdf_data)
        rp = ReportParser(strip_headers(pdf_text))
