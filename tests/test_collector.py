# Unit tests.  Run with: python3 -m unittest
from unittest import TestCase, main
from collector import MAIN_PAGE, get_pdf_urls, grab_pdf, get_text_from_pdf
from requests import get
try:
    from bs4 import BeautifulSoup
except ImportError as e:
    print("{}.  Try: pip3 install -r requirements.txt".format(str(e)))
    from sys import exit
    exit(1)

class TestStringMethods(TestCase):

    def test_get_pdf_urls(self):
        hrefs = get_pdf_urls(MAIN_PAGE)
        self.assertNotEqual(len(hrefs), 0)

    def test_grab_pdf(self):
        hrefs = get_pdf_urls(MAIN_PAGE)
        pdf_data = grab_pdf(hrefs[0])
        self.assertNotEqual(pdf_data, None)
        self.assertNotEqual(pdf_data, "")

    def test_get_text_from_pdf(self):
        hrefs = get_pdf_urls(MAIN_PAGE)
        pdf_data = grab_pdf(hrefs[0])
        pdf_text = get_text_from_pdf(pdf_data)
        self.assertNotEqual(pdf_text, None)
        self.assertNotEqual(pdf_text, "")

if __name__ == '__main__':
    main()
