# UPD Stats
This software will scrape the media reports from the Urbana Police Department's
website, extract the text, parse it into fields, and put it into a database so
it can be analyzed.

These media reports and anyone can run this software to collect this
information.  It could be used to better understand what types of crimes are
committed, see trends over time, determine if budget increase or cuts have an
impact on arrests made, crimes reported, or a variety of other things.  It
could also be used to find officers who are doing an outstanding job and see
things like how many cases each officer handles per month, how many arrests
are made, how many crimes go unsolved, and so forth.

# Alternative
There's an official [site](https://data.urbanaillinois.us) which contains
similar police data and a lot of other info, in an easier to use format and
with better tools to sift through the data.  This was not discovered until
after this project was at a stable point.  It's still useful for comparing
the media reports to what is on data.urbanaillinois.us, but inferior in nearly
every other manner.

# Getting Started
To start, you'll need to get the dependencies like this:
`pip3 install -r requirements.txt`

After this, you can run the unit tests with:
`python3 -m unittest`

Running the collector will download the PDFs:
`./collector.py`

These can be imported into the database with:
`./database.py archive/*.PDF`

The database can be queried with the command line sqlite client:
`sqlite3 upd.db`

See below for queries that you might be interested in running.

# Roadmap
The next feature to add is the ability to access this data via a webpage. The
point is to allow anyone to be able to use this data in meaningful ways as well
as remove the 90 day limit that the UPD imposes on their media reports.

# Known Issues
Sometimes the text can not be extracted from the PDF (at least by pdfminer).
When this happens, a warning is printed and that PDF is not imported.

When a page break occurs after a single line of a report, the text which is
extracted from the PDF, does not match the ordering of the text when it is
viewed with a PDF viewer.  This is not something that we are able to detect,
which means it will cause data to be incorrect.  Presumably this is caused by
the way the UPD creates these PDFs.  Example files that demonstrate this issue:

UPD3943.PDF
UPD3960.PDF
UPD4056.PDF
UPD4112.PDF

# Database
The database is just a SQLite database named upd.db.  The main table is the
reports table, but there are also person, charge, offender, victim, and arrest.
This is not all included in the report table because there can be an arbitrary
number of offenders per report, and one ofender could appear in multiple
reports.  This is also true of charges, victims and arrests that are made.

## Queries
Here are some database queries for reference.

-- See info about a case
select * from report WHERE case_number = "U20-01860";

-- See the alledged charges for a case
select * from charge WHERE report = (select id from report WHERE case_number = "U20-01860");

-- See the arrests made for a case
elect * from arrest WHERE report = (select id from report WHERE case_number = "U20-01860");

-- See all the arrests made on Vine St.
SELECT * FROM arrest WHERE at LIKE "%VINE%";

-- Get the case numbers for those arrests made on Vine St.
SELECT report.case_number FROM report, arrest WHERE report.id = arrest.report AND arrest.at LIKE "%VINE%";

-- See the arresting officer for all the arrests on Vine St.
SELECT person.name, arrest.charge_description, arrest.at FROM person, arrest WHERE person.id = arrest.officer AND arrest.at LIKE "%VINE%";

-- See arrests for posession (mixes drugs, paraphanalia, and stolen property)
SELECT person.name, arrest.charge_description, arrest.at FROM person, arrest WHERE person.id = arrest.officer AND arrest.charge_description LIKE "%POSESSION%";

-- See the top 10 things people are arrested for
select count( * ) as cnt, charge_description, charge_code FROM arrest GROUP BY charge_description ORDER by cnt DESC LIMIT 10;

-- See the people with the most charges against them
select count( * ) as cnt, person.name FROM arrest, person WHERE arrest.arrestee = person.id GROUP BY arrestee ORDER by cnt DESC LIMIT 10;

-- What percentage of crimes involve property damage/theft?
select CAST(count(property) as float)/count( * ) from report;

-- See the most common charges (note: alleged crimes, not arrests)
select count( * ) as cnt, charge, description from charge GROUP BY charge ORDER BY cnt DESC LIMIT 10;
