#!/usr/bin/env python3
try:
    from bs4 import BeautifulSoup
except ImportError as e:
    print("{}.  Try: pip3 install -r requirements.txt".format(str(e)))
    from sys import exit
    exit(1)


from argparse import ArgumentParser
from logging import basicConfig, debug, info, warning, error, critical, \
                    getLogger, DEBUG, INFO, WARNING, ERROR, CRITICAL
from os import listdir
from os.path import join
from requests import get
from report_parser import get_text_from_pdf, strip_headers, ReportParser
from urllib.parse import urlparse

ARCHIVE_PDFS = True
ARCHIVE_DIRECTORY = "archive"
BASE_URL = "https://www2.city.urbana.il.us"
MAIN_PAGE = "{}/_Police_Media_Reports/".format(BASE_URL)
LOG_FORMAT = "%(levelname)-8s %(asctime)-15s %(message)s"


def get_pdf_urls(url):
    """
    Extracts links to all of the PDFs from the given url

    :param url: The URL to fetch and use to find the links
    :type url: String
    :returns: List of URLs
    :rtype: List of Strings
    """
    hrefs = []
    response = get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    for link in soup.find_all('a'):
        href = link.get('href')
        if "PDF" in href:
            if href.startswith("/"):
                hrefs.append("{}{}".format(BASE_URL, href))
    return hrefs

def grab_pdf(url):
    """
    Grabs the requested PDF.  If ARCHIVE_PDFS is True, it will first check to
    see if the requested file is already on disk and grab it from the internet
    only if necessary (saving it to disk in the process).  If ARCHIVE_PDFS is
    false, it will unconditionally grab it from the internet.

    :param url: Location of the PDF file
    :type url: string
    :returns: The contents of the PDF
    :rtype: string
    """
    if ARCHIVE_PDFS:
        files = listdir(ARCHIVE_DIRECTORY)
        target = urlparse(url).path.split("/")[-1]
        if target in files:
            debug("Opening PDF: {}".format(target))
            with open(join(ARCHIVE_DIRECTORY, target), "rb") as f:
                content = f.read()
        else:
            debug("Downloading PDF: {}".format(url))
            content = get(url).content
            with open(join(ARCHIVE_DIRECTORY, target), "wb") as f:
                f.write(content)
    else:
        debug("Downloading PDF: {}".format(url))
        content = get(url).content
    return content

def handle_arguments():
    """
    Handle all the arguments, including setting log levels based on the -v/-q
    flags.

    :returns: arguments from the ArgumentParser
    :rtype: `py:argparse.Namespace` object
    """
    parser = ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--verbose', '-v', action='count', default=0)
    group.add_argument('--quiet', '-q', action='count', default=0)
    args = parser.parse_args()
    basicConfig(format=LOG_FORMAT, level=WARNING+args.quiet*10-args.verbose*10)
    # If you want more info about what pdfminer is doing, you'll have to change
    # the code.  This was done to prevent the pdfminer logs from drowning out
    # the messages from this program.
    getLogger("pdfminer").setLevel(WARNING)
    getLogger("requests").setLevel(WARNING) # Ditto for the requests library
    getLogger("urllib3").setLevel(WARNING) # and the underlying urllib3 library
    return args

if __name__ == "__main__":
    args = handle_arguments()
    hrefs = get_pdf_urls(MAIN_PAGE)
    for href in hrefs:
        pdf_data = grab_pdf(href)
        try:
            pdf_text = get_text_from_pdf(pdf_data)
        except Exception as e:
            warning("Unable to parse {} - {}".format(href, str(e)))
            continue
        rp = ReportParser(strip_headers(pdf_text))
