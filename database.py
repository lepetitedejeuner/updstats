#!/usr/bin/env python3
from argparse import ArgumentParser
from collector import LOG_FORMAT
from logging import basicConfig, debug, info, warning, error, critical, \
                    getLogger, DEBUG, INFO, WARNING, ERROR, CRITICAL
from report_parser import get_text_from_pdf, strip_headers, ReportParser
import sqlite3


FILENAME="upd.db"

def create_database(filename=FILENAME):
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS person (id INTEGER PRIMARY KEY ASC,
                  age integer, sex text, race text, height integer,
                  weight integer, hair text, eyes text, location text,
                  name text)''')
    c.execute('''CREATE TABLE IF NOT EXISTS report (id INTEGER PRIMARY KEY ASC,
                  case_number text, location text, occurred timestamp,
                  reported timestamp, officer integer, summary text,
                  property text, filename text,
                  FOREIGN KEY(officer) REFERENCES person(id))''')
    c.execute('''CREATE TABLE IF NOT EXISTS charge (id INTEGER PRIMARY KEY ASC,
                  report integer, description text, charge text, 
                  FOREIGN KEY(report) REFERENCES report(id))''')
    c.execute('''CREATE TABLE IF NOT EXISTS offender (id INTEGER PRIMARY KEY ASC,
                  report integer, offender integer,
                  FOREIGN KEY(offender) REFERENCES person(id),
                  FOREIGN KEY(report) REFERENCES report(id))''')
    c.execute('''CREATE TABLE IF NOT EXISTS victim (id INTEGER PRIMARY KEY ASC,
                  report integer, victim integer,
                  FOREIGN KEY(victim) REFERENCES person(id),
                  FOREIGN KEY(report) REFERENCES report(id))''')
    c.execute('''CREATE TABLE IF NOT EXISTS arrest (id INTEGER PRIMARY KEY ASC,
                  arrestee integer, charge_description text, charge_code text,
                  at text, officer integer, report integer,
                  FOREIGN KEY(arrestee) REFERENCES person(id),
                  FOREIGN KEY(officer) REFERENCES person(id),
                  FOREIGN KEY(report) REFERENCES report(id))''')
    return conn

def get_person(conn, person):
    query_args = []
    where = []
    query = ("SELECT id, age, sex, race, height, weight, hair, eyes, location, "
             "name FROM person WHERE ")
    if person.age == None:
        where.append("age IS NULL")
    else:
        where.append("age = ?")
        query_args.append(person.age)
    if person.sex == None:
        where.append("sex IS NULL")
    else:
        where.append("sex = ?")
        query_args.append(person.sex)
    if person.name == None:
        where.append("name IS NULL")
    else:
        where.append("name = ?")
        query_args.append(person.name)

    c = conn.cursor()
    c.execute(query + " AND ".join(where), query_args)
    return c.fetchone()

def get_report_id(conn, report):
    query = ("SELECT id, case_number, location, occurred, reported, officer, "
             "summary, property, officer from report WHERE case_number = ?")
    c = conn.cursor()
    c.execute(query, [report.case_number])
    return c.fetchone()

def insert_person(conn, person):
    c = conn.cursor()

    #debug("Inserting {} to person table: {}".format(person.name, str(person)))
    # First, see if we already have this person, and if so, return their ID
    person_record = get_person(conn, person)
    if person_record != None:
        return person_record[0]

    # They weren't in there, so now we need to add them
    c.execute(("INSERT INTO person(age, sex, race, height, weight, hair, eyes,"
               "location, name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"),
              (person.age, person.sex, person.race, person.height,
               person.weight, person.hair, person.eyes, person.location,
               person.name))
    conn.commit()
    return c.lastrowid # and return the person ID (row id)

def insert_victim(conn, victim, report_id):
    victim_id = insert_person(conn, victim)
    c = conn.cursor()
    c.execute("INSERT INTO victim(victim, report) VALUES (?, ?)",
               [victim_id, report_id])
    conn.commit()
    return c.lastrowid

def insert_offender(conn, offender, report_id):
    offender_id = insert_person(conn, offender)
    c = conn.cursor()
    c.execute("INSERT INTO offender(offender, report) VALUES (?, ?)",
               [offender_id, report_id])
    conn.commit()
    return c.lastrowid

def insert_arrest(conn, arrest, report_id):
    arrestee_id = insert_person(conn, arrest.arrestee)
    officer_id = insert_person(conn, arrest.officer)
    c = conn.cursor()
    c.execute(("INSERT INTO arrest(arrestee, charge_description, charge_code, "
               "at, officer, report) VALUES (?, ?, ?, ?, ?, ?)"),
              [arrestee_id, arrest.charge[0], arrest.charge[1], arrest.at,
               officer_id, report_id])
    conn.commit()
    return c.lastrowid

def insert_charges(conn, arrest):
    c = conn.cursor()
    raise Exception("Not implemented")
    return 1  # TODO: return arrest ID

def insert_report(conn, report, officer_id, filename):
    # First, see if we already have this report, we're done here
    report_record = get_report_id(conn, report)
    if report_record:
        return report_record[0]

    c = conn.cursor()
    c.execute(("INSERT INTO report(case_number, location, occurred, reported, "
              "officer, summary, property, filename) VALUES (?, ?, ?, ?, ?, ?, "
              "?, ?)"),
              [report.case_number, report.location, report.occurred,
               report.reported, officer_id, report.summary, report.property,
               filename])
    conn.commit()
    report_id = c.lastrowid
    return report_id

def insert_charge(conn, charge, report_id):
    c = conn.cursor()
    c.execute("INSERT INTO charge(description, charge, report) VALUES (?, ?, ?)",
        [charge[0], charge[1], report_id])
    conn.commit()
    return c.lastrowid

def process_report(conn, report, filename):
    officer_id = insert_person(conn, report.officer)
    report_id = insert_report(conn, report, officer_id, filename)

    for charge in report.charges:
        insert_charge(conn, charge, report_id)
    for offender in report.offenders:
        insert_offender(conn, offender, report_id)
    for victim in report.victims:
        insert_victim(conn, victim, report_id)
    for arrest in report.arrests:
        insert_arrest(conn, arrest, report_id)

def handle_arguments():
    parser = ArgumentParser()
    parser.add_argument('file', nargs="+")
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--verbose', '-v', action='count', default=0)
    group.add_argument('--quiet', '-q', action='count', default=0)
    args = parser.parse_args()

    basicConfig(format=LOG_FORMAT, level=WARNING+args.quiet*10-args.verbose*10)
    # If you want more info about what pdfminer is doing, you'll have to change
    # the code.  This was done to prevent the pdfminer logs from drowning out
    # the messages from this program.
    getLogger("pdfminer").setLevel(WARNING)
    getLogger("requests").setLevel(WARNING) # Ditto for the requests library
    getLogger("urllib3").setLevel(WARNING) # and the underlying urllib3 library
    return args

if __name__ == "__main__":
    args = handle_arguments()

    conn = create_database()
    for f in args.file:
        with open(f, "rb") as fh:
            pdf_data = fh.read()
        try:
            pdf_text = get_text_from_pdf(pdf_data)
            debug(pdf_text)
        except Exception as e:
            warning("Unable to parse {} - {}".format(f, str(e)))
            continue
        rp = ReportParser(strip_headers(pdf_text))
        for report in rp.reports:
            process_report(conn, report, f)
